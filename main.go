package main

import (
	"fmt"
	"os"

	"github.com/bwmarrin/discordgo"
)

func main() {
	session, _ := discordgo.New("Bot " + os.Args[1])
	session.LogLevel = discordgo.LogDebug
	session.AddHandler(messageCreate)
	session.Open()
	<-make(chan struct{})
	return
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Content == "joinVoice" {
		fmt.Println("joining voice")
		channel, _ := s.Channel(m.ChannelID)
		guild, _ := s.Guild(channel.GuildID)
		for _, state := range guild.VoiceStates {
			if state.UserID == m.Author.ID {
				_, err := s.ChannelVoiceJoin(state.GuildID, state.ChannelID, false, false)
				if err != nil {
					fmt.Println(err)
				}
			}
		}
	}
}
