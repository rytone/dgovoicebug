steps to reproduce:
 - join a voice channel
 - start bot (`dgovoicebug [token]`)
 - send a message with the content "joinVoice"
 - observe it not joining and instead reconnecting

![](https://files.rytone.tk/Hfn7RFFu2GeyiAt2.png)
